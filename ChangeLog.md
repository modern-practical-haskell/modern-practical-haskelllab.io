# Changelog for shakebook

## v0.1.0.0

* Skeleton Shake application that can export technical documentation both to
  HTML and to PDF
