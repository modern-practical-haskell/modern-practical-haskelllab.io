---
title: "Understanding Types"
author: Me
date: Mar 28, 2020
tags: [shakebook]
description: What do we mean by a type?
---

# Understanding Types

Programing languages today are generally divided into two major paradigms, imperative languages and declarative languages and it’s important to understand what that means. The atomic sentence in an imperative language is the instruction, it tells the computer what to do i.e;

* add 2 to 5 and store it as ‘x’
* stack three balls of snow on top of each other, add twigs on each side of the middle one and a carrot and two pebbles on the front of the top one
* when the acclerator is pressed, increase the speed of the car by x every frame

The atomic sentence of declarative languages is the expression, it tells the computer what something is i.e;

* x is the sum of 2 and 5
* a snowman is a stack of three balls of snow, with twigs for arms, a carrot for a nose and pebbles for eyes.
* the speed of the car is a function of the acceleration and the time for which it has been accelerating

Haskell is a declarative language. All statements in Haskell are expressions, not instructions, although some expression can be made to look like instructions. Some of these expressions declare types, for example the Snowman in the above example could be considered a type, and any individual snowman can be considered a value of the type Snowman. Types are a very effective idea in programming precisely because human categorization systems are so incredibly old and immensely sophisticated and powerful. Categorisation helps us determine which berries will kill us if we eat them, what material to use to build homes out of, and it doesn’t stop there; even small organisms can distinguish between light and dark and can identify a sodium molecule and decide what to do about it. Categorization is perhaps the oldest form of cognitive process and gives a staggering amount of information, not just about how things appear to us but, and perhaps even more primordially, how they can be used and what their effects are on us and on the world; and one of the main themes of the philosophy of Haskell is to give priority to describing precisely just what the thing is that we are talking about, with greater and greater specificity.

The key insight here is that evaluating an expression is itself a computation, we sometimes say the result is synthesized. We come up with a value for a computation by evaluating it, by in turn evaluating all of its subexpressions. A function is an expression too, one that expressed a mechanism for taking a set of input values of certain types and relating them by a transformation to an output of another type. Here is a function called f, that takes an argument x of type Int, and returns an Int, (noted by the type signature Int -> Int)

```{.haskell}
f :: Int -> Int
f x = x + 1
```

Once we have declared this function, then wherever we come across the symbols f x, where x is an integer, we know this will be replaced with the function body x + 1. If we try to use a value of the wrong type in a function, Haskell will not compile it for us. We can use types to indicate to ourselves and to the compiler exactly which values will make sense in which expressions, by restricting what can be used as input to values of specific types. For that, we need to understand how to make our own types, and practice using them in functions.
