---
title: "Haskell Tooling"
author: Me
date: Mar 28, 2020
tags: [shakebook]
description: Getting Started with Haskell
---

# Haskell Tooling

This section covers the compiler and Haskell build systems in use by the
community. There are three build systems that people frequently use - cabal,
stack and nix. This section covers them all. If you don't care about all of
them, we recommend `stack` to start with.

## cabal

* TODO
## stack

* TODO

## nix

* TODO
