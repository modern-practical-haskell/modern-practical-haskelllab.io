---
title: "Absolute Beginner"
author: Me
date: Mar 28, 2020
tags: [shakebook]
description: Getting Started with Haskell
---

# Abolute Beginner

This section deals with the tools you will need to get to grips with the Haskell
ecosystem, how to think about types, and how to get a basic application up and
running.
