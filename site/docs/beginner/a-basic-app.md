---
title: "A Basic App"
author: Me
date: Mar 28, 2020
tags: [shakebook]
description: A first look at computation
---

# A Basic App

Let’s take a look at some simple ideas. Create a new project called dragons with stack new dragons. The following code you can put in your app/Main.hs file.

```{.haskell}
data Caterpillar = Caterpillar deriving Show
data Butterfly = Butterfly deriving Show

cocoon :: Caterpillar -> Butterfly
cocoon Caterpillar = Butterfly
```

You can then change your main function to look like this:

```{.haskell}
main :: IO ()
main = do
  let x = Caterpillar 
  print x
  print $ cocoon x
```

Don’t have two main functions or you’ll get an error. You can run this with

```{.bash}
stack build
stack exec -- dragons-exe
```

You should get the output

```{.bash} 
Caterpillar
Butterfly
```

Let’s disect this, we have two data declarations; Butterfly and Caterpillar, and we have two function declarations; cocoon and main.

A data declaration declares a type with all of its potential values. In a data declaration, a type constructor is the thing on the left hand side of the equals sign. The data constructor(s) are the things on the right hand side of the equals sign. You use type constructors where a type is expected, and you use data constructors where a value is expected. We reason about types and values differently and this distinction is important to make clear upfront. In this case, the type Butterfly has one and only one value, also called ‘Butterfly’, and likewise with Caterpillar. Later we will make types that have several values, for now they are boring.

The deriving Show part will make sure that values of our type can be printed to the terminal, use this on your declarations where you can.

cocoon is a function, it turns a value of the type caterpillar in to a value of the type butterfly. Functions come in two parts, a type signature and an implementation. The first line is the type signature, it indicates that this is a function from Caterpillars to Butterflys. The second line is the implementation, it says that wherever you see the expression cocoon Caterpillar, you may replace it with the value Butterfly, and wherever you see the value Butterfly you may replace it with the value cocoon Caterpillar. This is called equational reasoning.

We also say that the function ‘cocoon’ acts on the value Caterpillar to produce the value Butterfly.

The main function will print things out for us, we assign x the value Caterpillar, and then print x to the console, then we evaluate cocoon x, and print that to the terminal. Since x is equal to Caterpillar, and cocoon Caterpillar is equal to Butterfly, we print the value Butterfly. The dollar sign $ means "evaluate everything on the right first, and then pass the result to the function on the left hand side. We’ll say more about this later.

So far our types aren’t very interesting due to them only having one value, let’s add some more complex types.
 
```{.haskell} 
data Dragon = Dragon {
  name :: String,
  heads :: Int
} deriving Show

This type Dragon is called a product type. It has to be constructed using the value constructor Dragon and supplied two additional values of specific types. It requires a String for a name, and an Int to determine what number of heads it has. We can make two dragons like so: (add the following to your do block in the main function, remember - lines in the do block must have the correct indentation).

```{.haskell}
let a = Dragon { name = "Bob", heads = 2 }
let b = Dragon { name = "Schmebulok", heads = 3 }
print a
print b
print $ name a -- prints "Bob"
print $ heads b -- prints 3
```

Here we have constructed a couple of Dragons, one named Bob with two heads, and one named Schmebulok with three heads. We assign these algebraic variable names a and b. Because our Dragon derives ‘Show’, we can print the whole Dragon value with all of its component values. The field accessors name and heads define functions with which we can extract particular values from a Dragon and print those out separately.

Let’s give our Dragon type a little more information, by adding a new type Color, and giving Dragons a color attribute. Add the Color type to your code:

```{.haskell}
data Color = Red | Green | Blue deriving Show
```

The Color type has a range of possible values - Red, Green and Blue. This is called a sum type, it indicates that any of those three values can be considered a value of type Color. Lets modify the dragon type so that it is defined with a color.

```{.haskell}
data Dragon = Dragon {
  name :: String,
  heads :: Int,
  color :: Color
} deriving Show
```

And let’s change Bob and Schmebulok so that they have a color. We need to do this for the application to succesfully compile.

```{.haskell}
let a = Dragon { name = "Bob", heads = 2, color = Blue }
let b = Dragon { name = "Schmebulok", heads = 3, color = Red }
```

Now the Dragons will fight each other. Let’s make a function called fight that will take two dragons and return the “winner”, the dragon that has the most heads.

```{.haskell}
fight :: Dragon -> Dragon -> Dragon
fight x y = if heads x > heads y then x else y
```

This type signature has two possible interpretations. The first is that fight is a function that accepts two arguments (both Dragons) and returns one of the Dragons. The second interpretation is that of a function that accepts one argument and returns a function that accepts the second argument and returns the final result. Both of these interpretations are fine, and it’s important to get a sense for both in context. For the moment, think of it as a two argument function.

The implementation of this function will compare the number of heads of x and y, and return the dragon that has the most heads. Calling this function with our two dragons and printing the result:

```{.haskell}
print $ fight a b
```

should print out Schmebulok as the winner of the fight.

You can get the code for this section here
