{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE NoImplicitPrelude         #-}
{-# LANGUAGE OverloadedStrings         #-}

module Main where

import           Control.Comonad
import           Control.Comonad.Cofree
import           Control.Lens               hiding ((:<))
import           Data.Aeson                 as A
import           Data.Aeson.Lens
import           Development.Shake
import           Development.Shake.Classes
import           Development.Shake.FilePath
import           Development.Shake.Forward
import           GHC.Generics               (Generic)
import           RIO
import qualified RIO.ByteString.Lazy        as LBS
import           RIO.Directory
import qualified RIO.HashMap                as HML
import           RIO.Partial
import qualified RIO.Text                   as T
import qualified RIO.Vector                 as V
import           Slick
import           Slick.Pandoc
import           Text.Pandoc.Class
import           Text.Pandoc.Definition
import           Text.Pandoc.Highlighting
import           Text.Pandoc.Options
import           Text.Pandoc.PDF
import           Text.Pandoc.Readers
import           Text.Pandoc.Templates
import           Text.Pandoc.Walk
import           Text.Pandoc.Writers


---Config-----------------------------------------------------------------------

withJSON :: (ToJSON a) => a -> Value -> Value
withJSON x (Object obj) = Object $ HML.union obj y
  where Object y = toJSON x
withJSON _ _ = error "Can only add a new ToJSON object to objects"

siteMeta :: SiteMeta
siteMeta = SiteMeta { author = "Me"
                    , title = "Modern Practical Haskell"
                    , baseUrl = "https://modern-practical-haskell.gitlab.io"
                    }

outputFolder :: FilePath
outputFolder = "public/"

toc :: ToC
toc = "site/docs/index.md" :< [
         "site/docs/beginner/index.md" :< [
           "site/docs/beginner/haskell-tooling.md" :< []
         , "site/docs/beginner/understanding-types.md" :< []
         , "site/docs/beginner/a-basic-app.md" :< []
         ]
       ]

animationFormat = "mp4"

--Data models-------------------------------------------------------------------

data SiteMeta = SiteMeta
  { author :: Text
  , title :: Text
  , baseUrl :: Text
  } deriving (Generic, Eq, Ord, Show, ToJSON)

type ToC = Cofree [] String

withHighlighting :: Value -> Value
withHighlighting = _Object . at "highlighting-css" ?~ String (T.pack $ styleToCss pygments)

addSubsectionsToTarget :: Cofree [] Value -> Value
addSubsectionsToTarget (x :< xs) = _Object . at "subsections" ?~ Array (V.fromList $ map extract xs) $ x
 
extendDocData :: Cofree [] Value -> Cofree [] Value
extendDocData = extend addSubsectionsToTarget

getDocData :: String -> Action Value
getDocData srcPath = do
  docContent <- readFile' srcPath
  docData <- markdownToHTMLWithOpts markdownReaderOptions html5WriterOptions . T.pack $ docContent
  let postUrl     = T.pack . ("/" <>) . dropDirectory1 $ srcPath -<.> "html"
      withPostUrl = _Object . at "url" ?~ String postUrl
      withSrcPath = _Object . at "srcPath" ?~ String (T.pack srcPath)
  return $ withPostUrl $ withSrcPath $ docData

tocNavbarData :: Cofree [] Value -> Value
tocNavbarData (x :< xs) = 
  object ["toc1" A..= [_Object . at "toc2" ?~ Array (V.fromList $ map toc2 xs) $ x]] where
      toc2 (x :< xs) = (_Object . at "toc3" ?~ Array (V.fromList $ map extract xs)) x

blogNavbarData :: [Value] -> Value
blogNavbarData xs = object ["toc1" A..= object [ "title" A..= String "Blog", "url" A..= String "/posts" , "toc2" A..= Array (V.fromList xs) ] ]

--Pandoc Configuration----------------------------------------------------------

markdownReaderOptions :: ReaderOptions
markdownReaderOptions = def { readerExtensions = pandocExtensions }

html5WriterOptions :: WriterOptions
html5WriterOptions = def { writerHTMLMathMethod = MathJax ""}

latexWriterOptions :: WriterOptions
latexWriterOptions = def { writerTableOfContents = True }

makePDFLaTeX :: Pandoc -> PandocIO LBS.ByteString
makePDFLaTeX p = do
  t <- getDefaultTemplate "latex"
  makePDFThrow "pdflatex" [] writeLaTeX latexWriterOptions { writerTemplate = Just t } p

makePDFThrow :: String -> [String] -> (WriterOptions -> Pandoc -> PandocIO Text) -> WriterOptions -> Pandoc -> PandocIO LBS.ByteString
makePDFThrow a b c d e = makePDF a b c d e >>= either (fail . show) return

-- Build Rules-----------------------------------------------------------------

buildPage :: FilePath -> FilePath -> Value -> Action Value
buildPage template outPath pageData = do
  pageT <- compileTemplate' template
  writeFile' (outputFolder </> outPath) . T.unpack $ substitute pageT $ withHighlighting $ withJSON (object [ "baseUrl" A..= baseUrl siteMeta ]) $ pageData
  return pageData

buildIndex :: Action ()
buildIndex = void $ buildPage "site/templates/index.html" "index.html" (toJSON siteMeta)

buildDoc :: FilePath -> Value -> Action Value
buildDoc = buildPage "site/templates/docs.html"

buildPost :: FilePath -> Value -> Action Value
buildPost = buildPage "site/templates/post.html"

buildPostIndex :: Value -> Action ()
buildPostIndex = void . buildPage "site/templates/post-list.html" "posts/index.html"

getFPDataPair :: Cofree [] String -> Action (String, Value)
getFPDataPair toc@(x :< xs) = do
  t <- traverse getDocData toc
  return (dropDirectory1 x -<.> "html", extract $ extendDocData t)

buildDocs :: Cofree [] String -> Action ()
buildDocs toc = do
  xs <- sequence $ toc =>> getFPDataPair
  let t = tocNavbarData (snd <$> xs)
  traverse_ (uncurry buildDoc) (second (withJSON t) <$> xs)

buildPosts :: Action ()
buildPosts = do
  filepaths <- getDirectoryFiles "./site/" ["posts//*.md"]
  putNormal $ show filepaths
  xs <- forM filepaths $ \x -> do
    d <- getDocData ("site" </> x)
    return (x -<.> "html", d)
  let t = blogNavbarData (snd <$> xs)
  ys <- traverse (uncurry buildPost) ((second (withJSON t) <$> xs))
  buildPostIndex $ withJSON t $  object [ "posts" A..= Array (V.fromList ys), "title" A..= String "Posts" ]

handleImages :: Inline -> Inline
handleImages x@(Image attr ins (src,txt)) =
  if (T.takeEnd 4 (T.pack src) == ".mp4") then Str (T.unpack $ "[Video Available At " <> baseUrl siteMeta <> T.pack src <> "]")
  else Image attr ins ("public/" ++ src, txt)
handleImages x = x

handleHeaders :: Int -> Block -> Block
handleHeaders i x@(Header a as xs) = Header (max 1 (a + i)) as xs
handleHeaders _ x = x

pushHeaders :: Int -> Cofree [] Pandoc -> Cofree [] Pandoc
pushHeaders i (x :< xs) = walk (handleHeaders i) x :< map (pushHeaders (i+1)) xs

buildPDF :: FilePath -> Action ()
buildPDF x = do
  y <- mapM readFile' toc
  f <- liftIO . runIOorExplode $ do
    k <- mapM (readMarkdown markdownReaderOptions . T.pack) y
    let z = walk handleImages $ foldr (<>) mempty $ pushHeaders (-1) k
    makePDFLaTeX z
  LBS.writeFile x f

copyStaticFiles :: Action ()
copyStaticFiles = do
  filepaths <- getDirectoryFiles "./site/" ["images//*", "css//*", "js//*", "webfonts//*"]
  void $ forP filepaths $ \filepath ->
    copyFileChanged ("site" </> filepath) (outputFolder </> filepath)

buildAnimation :: FilePath -> Action ()
buildAnimation srcPath = cacheAction ("build" :: T.Text, srcPath) $ do
    command_ [] "runhaskell" [srcPath, "render", "--format", animationFormat, "--raster", "convert", "--preset", "quick"]
    copyFileChanged (srcPath -<.> animationFormat) (outputFolder </> (dropDirectory1 $ srcPath -<.> animationFormat))

buildRPlot :: FilePath -> Action ()
buildRPlot srcPath = cacheAction ("build" :: T.Text, srcPath) $ do
    command_ [] "runhaskell" [srcPath, "-o", (srcPath -<.> ".png")]
    copyFileChanged (srcPath -<.> ".png") (outputFolder </> (dropDirectory1 $ srcPath -<.> ".png"))

buildRules :: Action ()
buildRules = do
  buildAnimation "site/animations/latex_draw.hs"
  buildRPlot "site/plots/cluster.hs"
--  buildAnimation "site/animations/tut_glue_blender.hs"
  buildDocs toc
  buildPosts
  buildIndex
  buildPDF "public/book.pdf"
  copyStaticFiles

main :: IO ()
main = do
  let shOpts = forwardOptions $ shakeOptions { shakeVerbosity = Chatty, shakeLintInside = ["\\"]}
  shakeArgsForward shOpts buildRules
