{ pkgs ? import ./nix/fetch-nixpkgs.nix {} }:

let
  hsPkgs = import ./nix/default.nix;
in
  hsPkgs.shellFor {
    withHoogle = true;

    packages = ps: with ps; [shakebook];
    buildInputs = with pkgs.haskellPackages;
      [ hlint stylish-haskell ghcid pkgs.ffmpeg-full pkgs.blender pkgs.texlive.combined.scheme-full pkgs.imagemagick pkgs.shake pkgs.zlib.out pkgs.pkg-config pkgs.librsvg pkgs.fsatrace wai-app-static pkgs.R];

    LD_LIBRARY_PATH="${pkgs.zlib.out}/lib";
    exactDeps = true;
  }
